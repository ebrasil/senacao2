﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace Senacao.Data
{
    public interface ISQLite
    {
        SQLiteConnection Conexao();
    }
}
